/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.service;

import com.totok.entity.Karyawan;
import com.totok.entity.Tpengguna;
import com.totok.service.KaryawanDAO;
import com.totok.service.PenggunaDAO;
import com.totok.util.DaoFactory;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.print.attribute.standard.Media;

/**
 *
 * @author Bowo.Susanto
 */
@WebService(serviceName = "NewWebService")
public class NewWebService {

    private KaryawanDAO karyawanService;

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     *
     * @return
     * @throws java.sql.SQLException
     */
    @WebMethod(operationName = "getKaryawan")
    public List<Karyawan> getKaryawan() {
        List<Karyawan> list = new ArrayList<>();
        try {
            KaryawanDAO karyawan = DaoFactory.getKaryawanService();
            list = karyawan.getAllKaryawan();
        } catch (Exception e) {
            System.err.println(e);
        }
        return list;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "Tambah")
    public Double Tambah(@WebParam(name = "a") Double a, @WebParam(name = "b") Double b) {
        //TODO write your implementation code here:
        Double hasil = a+b;
        return hasil;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "KirimFoto")
    public Media KirimFoto(@WebParam(name = "foto") Media foto) {
        //TODO write your implementation code here:
        return null;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "isPengguna")
    public Boolean isPengguna(@WebParam(name = "nama_pengguna") String nama_pengguna, @WebParam(name = "password") String password) {
        //TODO write your implementation code here:
        Boolean status = false;
        try {
            Tpengguna t = new Tpengguna();
            t.setNamaPengguna(nama_pengguna);
            t.setPassword(password);
            PenggunaDAO penggunaService = DaoFactory.getPenggunaService();
            status = penggunaService.getPengguna(t);
        } catch (Exception e) {
        }
        return status;
    }
}
