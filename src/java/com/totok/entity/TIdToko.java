/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "t_id_toko")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TIdToko.findAll", query = "SELECT t FROM TIdToko t"),
    @NamedQuery(name = "TIdToko.findByNamaToko", query = "SELECT t FROM TIdToko t WHERE t.namaToko = :namaToko"),
    @NamedQuery(name = "TIdToko.findByAlamat", query = "SELECT t FROM TIdToko t WHERE t.alamat = :alamat"),
    @NamedQuery(name = "TIdToko.findByKecamatan", query = "SELECT t FROM TIdToko t WHERE t.kecamatan = :kecamatan"),
    @NamedQuery(name = "TIdToko.findByKota", query = "SELECT t FROM TIdToko t WHERE t.kota = :kota"),
    @NamedQuery(name = "TIdToko.findByTelepon", query = "SELECT t FROM TIdToko t WHERE t.telepon = :telepon"),
    @NamedQuery(name = "TIdToko.findByNpwp", query = "SELECT t FROM TIdToko t WHERE t.npwp = :npwp")})
public class TIdToko implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nama_toko")
    private String namaToko;
    @Size(max = 60)
    @Column(name = "alamat")
    private String alamat;
    @Size(max = 30)
    @Column(name = "kecamatan")
    private String kecamatan;
    @Size(max = 30)
    @Column(name = "kota")
    private String kota;
    @Size(max = 20)
    @Column(name = "telepon")
    private String telepon;
    @Size(max = 30)
    @Column(name = "npwp")
    private String npwp;

    public TIdToko() {
    }

    public TIdToko(String namaToko) {
        this.namaToko = namaToko;
    }

    public String getNamaToko() {
        return namaToko;
    }

    public void setNamaToko(String namaToko) {
        this.namaToko = namaToko;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (namaToko != null ? namaToko.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TIdToko)) {
            return false;
        }
        TIdToko other = (TIdToko) object;
        if ((this.namaToko == null && other.namaToko != null) || (this.namaToko != null && !this.namaToko.equals(other.namaToko))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.TIdToko[ namaToko=" + namaToko + " ]";
    }
    
}
