/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "tbarang")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tbarang.findAll", query = "SELECT t FROM Tbarang t"),
    @NamedQuery(name = "Tbarang.findByKodeBarang", query = "SELECT t FROM Tbarang t WHERE t.kodeBarang = :kodeBarang"),
    @NamedQuery(name = "Tbarang.findByJenisBarang", query = "SELECT t FROM Tbarang t WHERE t.jenisBarang = :jenisBarang"),
    @NamedQuery(name = "Tbarang.findByNamaBarang", query = "SELECT t FROM Tbarang t WHERE t.namaBarang = :namaBarang"),
    @NamedQuery(name = "Tbarang.findByHargaBeli", query = "SELECT t FROM Tbarang t WHERE t.hargaBeli = :hargaBeli"),
    @NamedQuery(name = "Tbarang.findByHargaJual", query = "SELECT t FROM Tbarang t WHERE t.hargaJual = :hargaJual"),
    @NamedQuery(name = "Tbarang.findByStok", query = "SELECT t FROM Tbarang t WHERE t.stok = :stok"),
    @NamedQuery(name = "Tbarang.findBySatuan", query = "SELECT t FROM Tbarang t WHERE t.satuan = :satuan"),
    @NamedQuery(name = "Tbarang.findByMerk", query = "SELECT t FROM Tbarang t WHERE t.merk = :merk"),
    @NamedQuery(name = "Tbarang.findByHargaJual1", query = "SELECT t FROM Tbarang t WHERE t.hargaJual1 = :hargaJual1"),
    @NamedQuery(name = "Tbarang.findByIsi", query = "SELECT t FROM Tbarang t WHERE t.isi = :isi")})
public class Tbarang implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "kode_barang")
    private String kodeBarang;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 35)
    @Column(name = "jenis_barang")
    private String jenisBarang;
    @Size(max = 50)
    @Column(name = "nama_barang")
    private String namaBarang;
    @Column(name = "harga_beli")
    private Integer hargaBeli;
    @Column(name = "harga_jual")
    private Integer hargaJual;
    @Basic(optional = false)
    @NotNull
    @Column(name = "stok")
    private short stok;
    @Size(max = 20)
    @Column(name = "satuan")
    private String satuan;
    @Size(max = 50)
    @Column(name = "merk")
    private String merk;
    @Lob
    @Column(name = "satuan1")
    private byte[] satuan1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "harga_jual1")
    private int hargaJual1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isi")
    private short isi;

    public Tbarang() {
    }

    public Tbarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public Tbarang(String kodeBarang, String jenisBarang, short stok, int hargaJual1, short isi) {
        this.kodeBarang = kodeBarang;
        this.jenisBarang = jenisBarang;
        this.stok = stok;
        this.hargaJual1 = hargaJual1;
        this.isi = isi;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getJenisBarang() {
        return jenisBarang;
    }

    public void setJenisBarang(String jenisBarang) {
        this.jenisBarang = jenisBarang;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public Integer getHargaBeli() {
        return hargaBeli;
    }

    public void setHargaBeli(Integer hargaBeli) {
        this.hargaBeli = hargaBeli;
    }

    public Integer getHargaJual() {
        return hargaJual;
    }

    public void setHargaJual(Integer hargaJual) {
        this.hargaJual = hargaJual;
    }

    public short getStok() {
        return stok;
    }

    public void setStok(short stok) {
        this.stok = stok;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public byte[] getSatuan1() {
        return satuan1;
    }

    public void setSatuan1(byte[] satuan1) {
        this.satuan1 = satuan1;
    }

    public int getHargaJual1() {
        return hargaJual1;
    }

    public void setHargaJual1(int hargaJual1) {
        this.hargaJual1 = hargaJual1;
    }

    public short getIsi() {
        return isi;
    }

    public void setIsi(short isi) {
        this.isi = isi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kodeBarang != null ? kodeBarang.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tbarang)) {
            return false;
        }
        Tbarang other = (Tbarang) object;
        if ((this.kodeBarang == null && other.kodeBarang != null) || (this.kodeBarang != null && !this.kodeBarang.equals(other.kodeBarang))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.Tbarang[ kodeBarang=" + kodeBarang + " ]";
    }
    
}
