/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "tbayarhutang")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tbayarhutang.findAll", query = "SELECT t FROM Tbayarhutang t"),
    @NamedQuery(name = "Tbayarhutang.findByNoFaktur", query = "SELECT t FROM Tbayarhutang t WHERE t.noFaktur = :noFaktur"),
    @NamedQuery(name = "Tbayarhutang.findByKodeSupplier", query = "SELECT t FROM Tbayarhutang t WHERE t.kodeSupplier = :kodeSupplier"),
    @NamedQuery(name = "Tbayarhutang.findByTglPembelian", query = "SELECT t FROM Tbayarhutang t WHERE t.tglPembelian = :tglPembelian"),
    @NamedQuery(name = "Tbayarhutang.findByTotalPembelian", query = "SELECT t FROM Tbayarhutang t WHERE t.totalPembelian = :totalPembelian"),
    @NamedQuery(name = "Tbayarhutang.findByJumlahBayar", query = "SELECT t FROM Tbayarhutang t WHERE t.jumlahBayar = :jumlahBayar"),
    @NamedQuery(name = "Tbayarhutang.findByTglBayar", query = "SELECT t FROM Tbayarhutang t WHERE t.tglBayar = :tglBayar"),
    @NamedQuery(name = "Tbayarhutang.findByDiskon", query = "SELECT t FROM Tbayarhutang t WHERE t.diskon = :diskon"),
    @NamedQuery(name = "Tbayarhutang.findByKeterangan", query = "SELECT t FROM Tbayarhutang t WHERE t.keterangan = :keterangan"),
    @NamedQuery(name = "Tbayarhutang.findByNamaPengguna", query = "SELECT t FROM Tbayarhutang t WHERE t.namaPengguna = :namaPengguna")})
public class Tbayarhutang implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "no_faktur")
    private String noFaktur;
    @Size(max = 4)
    @Column(name = "kode_supplier")
    private String kodeSupplier;
    @Column(name = "tgl_pembelian")
    @Temporal(TemporalType.DATE)
    private Date tglPembelian;
    @Column(name = "total_pembelian")
    private Integer totalPembelian;
    @Column(name = "jumlah_bayar")
    private Integer jumlahBayar;
    @Column(name = "tgl_bayar")
    @Temporal(TemporalType.DATE)
    private Date tglBayar;
    @Column(name = "diskon")
    private Integer diskon;
    @Size(max = 200)
    @Column(name = "keterangan")
    private String keterangan;
    @Size(max = 30)
    @Column(name = "nama_pengguna")
    private String namaPengguna;

    public Tbayarhutang() {
    }

    public Tbayarhutang(String noFaktur) {
        this.noFaktur = noFaktur;
    }

    public String getNoFaktur() {
        return noFaktur;
    }

    public void setNoFaktur(String noFaktur) {
        this.noFaktur = noFaktur;
    }

    public String getKodeSupplier() {
        return kodeSupplier;
    }

    public void setKodeSupplier(String kodeSupplier) {
        this.kodeSupplier = kodeSupplier;
    }

    public Date getTglPembelian() {
        return tglPembelian;
    }

    public void setTglPembelian(Date tglPembelian) {
        this.tglPembelian = tglPembelian;
    }

    public Integer getTotalPembelian() {
        return totalPembelian;
    }

    public void setTotalPembelian(Integer totalPembelian) {
        this.totalPembelian = totalPembelian;
    }

    public Integer getJumlahBayar() {
        return jumlahBayar;
    }

    public void setJumlahBayar(Integer jumlahBayar) {
        this.jumlahBayar = jumlahBayar;
    }

    public Date getTglBayar() {
        return tglBayar;
    }

    public void setTglBayar(Date tglBayar) {
        this.tglBayar = tglBayar;
    }

    public Integer getDiskon() {
        return diskon;
    }

    public void setDiskon(Integer diskon) {
        this.diskon = diskon;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNamaPengguna() {
        return namaPengguna;
    }

    public void setNamaPengguna(String namaPengguna) {
        this.namaPengguna = namaPengguna;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noFaktur != null ? noFaktur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tbayarhutang)) {
            return false;
        }
        Tbayarhutang other = (Tbayarhutang) object;
        if ((this.noFaktur == null && other.noFaktur != null) || (this.noFaktur != null && !this.noFaktur.equals(other.noFaktur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.Tbayarhutang[ noFaktur=" + noFaktur + " ]";
    }
    
}
