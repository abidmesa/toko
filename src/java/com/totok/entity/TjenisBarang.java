/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "tjenis_barang")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TjenisBarang.findAll", query = "SELECT t FROM TjenisBarang t"),
    @NamedQuery(name = "TjenisBarang.findByNoJenis", query = "SELECT t FROM TjenisBarang t WHERE t.noJenis = :noJenis"),
    @NamedQuery(name = "TjenisBarang.findByJenisBarang", query = "SELECT t FROM TjenisBarang t WHERE t.jenisBarang = :jenisBarang")})
public class TjenisBarang implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "no_jenis")
    private String noJenis;
    @Size(max = 25)
    @Column(name = "jenis_barang")
    private String jenisBarang;

    public TjenisBarang() {
    }

    public TjenisBarang(String noJenis) {
        this.noJenis = noJenis;
    }

    public String getNoJenis() {
        return noJenis;
    }

    public void setNoJenis(String noJenis) {
        this.noJenis = noJenis;
    }

    public String getJenisBarang() {
        return jenisBarang;
    }

    public void setJenisBarang(String jenisBarang) {
        this.jenisBarang = jenisBarang;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noJenis != null ? noJenis.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TjenisBarang)) {
            return false;
        }
        TjenisBarang other = (TjenisBarang) object;
        if ((this.noJenis == null && other.noJenis != null) || (this.noJenis != null && !this.noJenis.equals(other.noJenis))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.TjenisBarang[ noJenis=" + noJenis + " ]";
    }
    
}
