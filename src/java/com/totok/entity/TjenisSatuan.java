/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "tjenis_satuan")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TjenisSatuan.findAll", query = "SELECT t FROM TjenisSatuan t"),
    @NamedQuery(name = "TjenisSatuan.findByNoSatuan", query = "SELECT t FROM TjenisSatuan t WHERE t.noSatuan = :noSatuan"),
    @NamedQuery(name = "TjenisSatuan.findByJenisSatuan", query = "SELECT t FROM TjenisSatuan t WHERE t.jenisSatuan = :jenisSatuan")})
public class TjenisSatuan implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "no_satuan")
    private String noSatuan;
    @Size(max = 25)
    @Column(name = "jenis_satuan")
    private String jenisSatuan;

    public TjenisSatuan() {
    }

    public TjenisSatuan(String noSatuan) {
        this.noSatuan = noSatuan;
    }

    public String getNoSatuan() {
        return noSatuan;
    }

    public void setNoSatuan(String noSatuan) {
        this.noSatuan = noSatuan;
    }

    public String getJenisSatuan() {
        return jenisSatuan;
    }

    public void setJenisSatuan(String jenisSatuan) {
        this.jenisSatuan = jenisSatuan;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noSatuan != null ? noSatuan.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TjenisSatuan)) {
            return false;
        }
        TjenisSatuan other = (TjenisSatuan) object;
        if ((this.noSatuan == null && other.noSatuan != null) || (this.noSatuan != null && !this.noSatuan.equals(other.noSatuan))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.TjenisSatuan[ noSatuan=" + noSatuan + " ]";
    }
    
}
