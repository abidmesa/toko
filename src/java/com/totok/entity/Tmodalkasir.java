/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "tmodalkasir")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tmodalkasir.findAll", query = "SELECT t FROM Tmodalkasir t"),
    @NamedQuery(name = "Tmodalkasir.findByTanggal", query = "SELECT t FROM Tmodalkasir t WHERE t.tmodalkasirPK.tanggal = :tanggal"),
    @NamedQuery(name = "Tmodalkasir.findByJamMulai", query = "SELECT t FROM Tmodalkasir t WHERE t.tmodalkasirPK.jamMulai = :jamMulai"),
    @NamedQuery(name = "Tmodalkasir.findByIdPengguna", query = "SELECT t FROM Tmodalkasir t WHERE t.tmodalkasirPK.idPengguna = :idPengguna"),
    @NamedQuery(name = "Tmodalkasir.findByNamaPengguna", query = "SELECT t FROM Tmodalkasir t WHERE t.namaPengguna = :namaPengguna"),
    @NamedQuery(name = "Tmodalkasir.findByModalAwal", query = "SELECT t FROM Tmodalkasir t WHERE t.modalAwal = :modalAwal"),
    @NamedQuery(name = "Tmodalkasir.findByTerima", query = "SELECT t FROM Tmodalkasir t WHERE t.terima = :terima"),
    @NamedQuery(name = "Tmodalkasir.findByTotal", query = "SELECT t FROM Tmodalkasir t WHERE t.total = :total"),
    @NamedQuery(name = "Tmodalkasir.findByJamSelesai", query = "SELECT t FROM Tmodalkasir t WHERE t.jamSelesai = :jamSelesai"),
    @NamedQuery(name = "Tmodalkasir.findByKet", query = "SELECT t FROM Tmodalkasir t WHERE t.ket = :ket")})
public class Tmodalkasir implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TmodalkasirPK tmodalkasirPK;
    @Size(max = 30)
    @Column(name = "nama_pengguna")
    private String namaPengguna;
    @Column(name = "modal_awal")
    private Integer modalAwal;
    @Column(name = "terima")
    private Integer terima;
    @Column(name = "total")
    private Integer total;
    @Column(name = "jam_selesai")
    @Temporal(TemporalType.TIME)
    private Date jamSelesai;
    @Column(name = "ket")
    private Character ket;

    public Tmodalkasir() {
    }

    public Tmodalkasir(TmodalkasirPK tmodalkasirPK) {
        this.tmodalkasirPK = tmodalkasirPK;
    }

    public Tmodalkasir(Date tanggal, Date jamMulai, String idPengguna) {
        this.tmodalkasirPK = new TmodalkasirPK(tanggal, jamMulai, idPengguna);
    }

    public TmodalkasirPK getTmodalkasirPK() {
        return tmodalkasirPK;
    }

    public void setTmodalkasirPK(TmodalkasirPK tmodalkasirPK) {
        this.tmodalkasirPK = tmodalkasirPK;
    }

    public String getNamaPengguna() {
        return namaPengguna;
    }

    public void setNamaPengguna(String namaPengguna) {
        this.namaPengguna = namaPengguna;
    }

    public Integer getModalAwal() {
        return modalAwal;
    }

    public void setModalAwal(Integer modalAwal) {
        this.modalAwal = modalAwal;
    }

    public Integer getTerima() {
        return terima;
    }

    public void setTerima(Integer terima) {
        this.terima = terima;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Date getJamSelesai() {
        return jamSelesai;
    }

    public void setJamSelesai(Date jamSelesai) {
        this.jamSelesai = jamSelesai;
    }

    public Character getKet() {
        return ket;
    }

    public void setKet(Character ket) {
        this.ket = ket;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tmodalkasirPK != null ? tmodalkasirPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tmodalkasir)) {
            return false;
        }
        Tmodalkasir other = (Tmodalkasir) object;
        if ((this.tmodalkasirPK == null && other.tmodalkasirPK != null) || (this.tmodalkasirPK != null && !this.tmodalkasirPK.equals(other.tmodalkasirPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.Tmodalkasir[ tmodalkasirPK=" + tmodalkasirPK + " ]";
    }
    
}
