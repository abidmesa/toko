/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Bowo.Susanto
 */
@Embeddable
public class TmodalkasirPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "tanggal")
    @Temporal(TemporalType.DATE)
    private Date tanggal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "jam_mulai")
    @Temporal(TemporalType.TIME)
    private Date jamMulai;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "id_pengguna")
    private String idPengguna;

    public TmodalkasirPK() {
    }

    public TmodalkasirPK(Date tanggal, Date jamMulai, String idPengguna) {
        this.tanggal = tanggal;
        this.jamMulai = jamMulai;
        this.idPengguna = idPengguna;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Date getJamMulai() {
        return jamMulai;
    }

    public void setJamMulai(Date jamMulai) {
        this.jamMulai = jamMulai;
    }

    public String getIdPengguna() {
        return idPengguna;
    }

    public void setIdPengguna(String idPengguna) {
        this.idPengguna = idPengguna;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tanggal != null ? tanggal.hashCode() : 0);
        hash += (jamMulai != null ? jamMulai.hashCode() : 0);
        hash += (idPengguna != null ? idPengguna.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmodalkasirPK)) {
            return false;
        }
        TmodalkasirPK other = (TmodalkasirPK) object;
        if ((this.tanggal == null && other.tanggal != null) || (this.tanggal != null && !this.tanggal.equals(other.tanggal))) {
            return false;
        }
        if ((this.jamMulai == null && other.jamMulai != null) || (this.jamMulai != null && !this.jamMulai.equals(other.jamMulai))) {
            return false;
        }
        if ((this.idPengguna == null && other.idPengguna != null) || (this.idPengguna != null && !this.idPengguna.equals(other.idPengguna))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.TmodalkasirPK[ tanggal=" + tanggal + ", jamMulai=" + jamMulai + ", idPengguna=" + idPengguna + " ]";
    }
    
}
