/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "tpelanggan")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tpelanggan.findAll", query = "SELECT t FROM Tpelanggan t"),
    @NamedQuery(name = "Tpelanggan.findByKodePelanggan", query = "SELECT t FROM Tpelanggan t WHERE t.kodePelanggan = :kodePelanggan"),
    @NamedQuery(name = "Tpelanggan.findByNamaPelanggan", query = "SELECT t FROM Tpelanggan t WHERE t.namaPelanggan = :namaPelanggan"),
    @NamedQuery(name = "Tpelanggan.findByAlamat", query = "SELECT t FROM Tpelanggan t WHERE t.alamat = :alamat"),
    @NamedQuery(name = "Tpelanggan.findByTelepon", query = "SELECT t FROM Tpelanggan t WHERE t.telepon = :telepon")})
public class Tpelanggan implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "kode_pelanggan")
    private String kodePelanggan;
    @Size(max = 35)
    @Column(name = "nama_pelanggan")
    private String namaPelanggan;
    @Size(max = 50)
    @Column(name = "alamat")
    private String alamat;
    @Size(max = 15)
    @Column(name = "telepon")
    private String telepon;

    public Tpelanggan() {
    }

    public Tpelanggan(String kodePelanggan) {
        this.kodePelanggan = kodePelanggan;
    }

    public String getKodePelanggan() {
        return kodePelanggan;
    }

    public void setKodePelanggan(String kodePelanggan) {
        this.kodePelanggan = kodePelanggan;
    }

    public String getNamaPelanggan() {
        return namaPelanggan;
    }

    public void setNamaPelanggan(String namaPelanggan) {
        this.namaPelanggan = namaPelanggan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kodePelanggan != null ? kodePelanggan.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tpelanggan)) {
            return false;
        }
        Tpelanggan other = (Tpelanggan) object;
        if ((this.kodePelanggan == null && other.kodePelanggan != null) || (this.kodePelanggan != null && !this.kodePelanggan.equals(other.kodePelanggan))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.Tpelanggan[ kodePelanggan=" + kodePelanggan + " ]";
    }
    
}
