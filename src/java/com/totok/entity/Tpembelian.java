/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "tpembelian")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tpembelian.findAll", query = "SELECT t FROM Tpembelian t"),
    @NamedQuery(name = "Tpembelian.findByNoPembelian", query = "SELECT t FROM Tpembelian t WHERE t.noPembelian = :noPembelian"),
    @NamedQuery(name = "Tpembelian.findByNoFaktur", query = "SELECT t FROM Tpembelian t WHERE t.noFaktur = :noFaktur"),
    @NamedQuery(name = "Tpembelian.findByTglPembelian", query = "SELECT t FROM Tpembelian t WHERE t.tglPembelian = :tglPembelian"),
    @NamedQuery(name = "Tpembelian.findByKodeSupplier", query = "SELECT t FROM Tpembelian t WHERE t.kodeSupplier = :kodeSupplier"),
    @NamedQuery(name = "Tpembelian.findByType", query = "SELECT t FROM Tpembelian t WHERE t.type = :type"),
    @NamedQuery(name = "Tpembelian.findByTotal", query = "SELECT t FROM Tpembelian t WHERE t.total = :total"),
    @NamedQuery(name = "Tpembelian.findByDiskon", query = "SELECT t FROM Tpembelian t WHERE t.diskon = :diskon"),
    @NamedQuery(name = "Tpembelian.findByTermin", query = "SELECT t FROM Tpembelian t WHERE t.termin = :termin"),
    @NamedQuery(name = "Tpembelian.findByBatasTglDiskon", query = "SELECT t FROM Tpembelian t WHERE t.batasTglDiskon = :batasTglDiskon"),
    @NamedQuery(name = "Tpembelian.findByTglJtmp", query = "SELECT t FROM Tpembelian t WHERE t.tglJtmp = :tglJtmp"),
    @NamedQuery(name = "Tpembelian.findByLunas", query = "SELECT t FROM Tpembelian t WHERE t.lunas = :lunas"),
    @NamedQuery(name = "Tpembelian.findByNamaPengguna", query = "SELECT t FROM Tpembelian t WHERE t.namaPengguna = :namaPengguna")})
public class Tpembelian implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "no_pembelian")
    private String noPembelian;
    @Size(max = 20)
    @Column(name = "no_faktur")
    private String noFaktur;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tgl_pembelian")
    @Temporal(TemporalType.DATE)
    private Date tglPembelian;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "kode_supplier")
    private String kodeSupplier;
    @Size(max = 10)
    @Column(name = "type")
    private String type;
    @Column(name = "total")
    private Integer total;
    @Column(name = "diskon")
    private Integer diskon;
    @Size(max = 20)
    @Column(name = "termin")
    private String termin;
    @Column(name = "batas_tgl_diskon")
    @Temporal(TemporalType.DATE)
    private Date batasTglDiskon;
    @Column(name = "tgl_jtmp")
    @Temporal(TemporalType.DATE)
    private Date tglJtmp;
    @Column(name = "lunas")
    private Character lunas;
    @Size(max = 35)
    @Column(name = "nama_pengguna")
    private String namaPengguna;

    public Tpembelian() {
    }

    public Tpembelian(String noPembelian) {
        this.noPembelian = noPembelian;
    }

    public Tpembelian(String noPembelian, Date tglPembelian, String kodeSupplier) {
        this.noPembelian = noPembelian;
        this.tglPembelian = tglPembelian;
        this.kodeSupplier = kodeSupplier;
    }

    public String getNoPembelian() {
        return noPembelian;
    }

    public void setNoPembelian(String noPembelian) {
        this.noPembelian = noPembelian;
    }

    public String getNoFaktur() {
        return noFaktur;
    }

    public void setNoFaktur(String noFaktur) {
        this.noFaktur = noFaktur;
    }

    public Date getTglPembelian() {
        return tglPembelian;
    }

    public void setTglPembelian(Date tglPembelian) {
        this.tglPembelian = tglPembelian;
    }

    public String getKodeSupplier() {
        return kodeSupplier;
    }

    public void setKodeSupplier(String kodeSupplier) {
        this.kodeSupplier = kodeSupplier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getDiskon() {
        return diskon;
    }

    public void setDiskon(Integer diskon) {
        this.diskon = diskon;
    }

    public String getTermin() {
        return termin;
    }

    public void setTermin(String termin) {
        this.termin = termin;
    }

    public Date getBatasTglDiskon() {
        return batasTglDiskon;
    }

    public void setBatasTglDiskon(Date batasTglDiskon) {
        this.batasTglDiskon = batasTglDiskon;
    }

    public Date getTglJtmp() {
        return tglJtmp;
    }

    public void setTglJtmp(Date tglJtmp) {
        this.tglJtmp = tglJtmp;
    }

    public Character getLunas() {
        return lunas;
    }

    public void setLunas(Character lunas) {
        this.lunas = lunas;
    }

    public String getNamaPengguna() {
        return namaPengguna;
    }

    public void setNamaPengguna(String namaPengguna) {
        this.namaPengguna = namaPengguna;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noPembelian != null ? noPembelian.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tpembelian)) {
            return false;
        }
        Tpembelian other = (Tpembelian) object;
        if ((this.noPembelian == null && other.noPembelian != null) || (this.noPembelian != null && !this.noPembelian.equals(other.noPembelian))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.Tpembelian[ noPembelian=" + noPembelian + " ]";
    }
    
}
