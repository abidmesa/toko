/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "tpengguna")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tpengguna.findAll", query = "SELECT t FROM Tpengguna t"),
    @NamedQuery(name = "Tpengguna.findByIdPengguna", query = "SELECT t FROM Tpengguna t WHERE t.idPengguna = :idPengguna"),
    @NamedQuery(name = "Tpengguna.findByNamaPengguna", query = "SELECT t FROM Tpengguna t WHERE t.namaPengguna = :namaPengguna"),
    @NamedQuery(name = "Tpengguna.findByAlamat", query = "SELECT t FROM Tpengguna t WHERE t.alamat = :alamat"),
    @NamedQuery(name = "Tpengguna.findByTelepon", query = "SELECT t FROM Tpengguna t WHERE t.telepon = :telepon"),
    @NamedQuery(name = "Tpengguna.findByPassword", query = "SELECT t FROM Tpengguna t WHERE t.password = :password"),
    @NamedQuery(name = "Tpengguna.findByLevel", query = "SELECT t FROM Tpengguna t WHERE t.level = :level")})
public class Tpengguna implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "id_pengguna")
    private String idPengguna;
    @Size(max = 35)
    @Column(name = "nama_pengguna")
    private String namaPengguna;
    @Size(max = 50)
    @Column(name = "alamat")
    private String alamat;
    @Size(max = 15)
    @Column(name = "telepon")
    private String telepon;
    @Size(max = 15)
    @Column(name = "password")
    private String password;
    @Size(max = 15)
    @Column(name = "level")
    private String level;

    public Tpengguna() {
    }

    public Tpengguna(String idPengguna) {
        this.idPengguna = idPengguna;
    }

    public String getIdPengguna() {
        return idPengguna;
    }

    public void setIdPengguna(String idPengguna) {
        this.idPengguna = idPengguna;
    }

    public String getNamaPengguna() {
        return namaPengguna;
    }

    public void setNamaPengguna(String namaPengguna) {
        this.namaPengguna = namaPengguna;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPengguna != null ? idPengguna.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tpengguna)) {
            return false;
        }
        Tpengguna other = (Tpengguna) object;
        if ((this.idPengguna == null && other.idPengguna != null) || (this.idPengguna != null && !this.idPengguna.equals(other.idPengguna))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.Tpengguna[ idPengguna=" + idPengguna + " ]";
    }
    
}
