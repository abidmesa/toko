/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "tpenjualan")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tpenjualan.findAll", query = "SELECT t FROM Tpenjualan t"),
    @NamedQuery(name = "Tpenjualan.findByNoNota", query = "SELECT t FROM Tpenjualan t WHERE t.noNota = :noNota"),
    @NamedQuery(name = "Tpenjualan.findByTglNota", query = "SELECT t FROM Tpenjualan t WHERE t.tglNota = :tglNota"),
    @NamedQuery(name = "Tpenjualan.findBySubBayar", query = "SELECT t FROM Tpenjualan t WHERE t.subBayar = :subBayar"),
    @NamedQuery(name = "Tpenjualan.findByDiskon", query = "SELECT t FROM Tpenjualan t WHERE t.diskon = :diskon"),
    @NamedQuery(name = "Tpenjualan.findByPpn", query = "SELECT t FROM Tpenjualan t WHERE t.ppn = :ppn"),
    @NamedQuery(name = "Tpenjualan.findByTotalBayar", query = "SELECT t FROM Tpenjualan t WHERE t.totalBayar = :totalBayar"),
    @NamedQuery(name = "Tpenjualan.findByBayar", query = "SELECT t FROM Tpenjualan t WHERE t.bayar = :bayar"),
    @NamedQuery(name = "Tpenjualan.findByKembali", query = "SELECT t FROM Tpenjualan t WHERE t.kembali = :kembali"),
    @NamedQuery(name = "Tpenjualan.findByKodePelanggan", query = "SELECT t FROM Tpenjualan t WHERE t.kodePelanggan = :kodePelanggan"),
    @NamedQuery(name = "Tpenjualan.findByNamaPengguna", query = "SELECT t FROM Tpenjualan t WHERE t.namaPengguna = :namaPengguna")})
public class Tpenjualan implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "no_nota")
    private String noNota;
    @Column(name = "tgl_nota")
    @Temporal(TemporalType.DATE)
    private Date tglNota;
    @Column(name = "sub_bayar")
    private Integer subBayar;
    @Column(name = "diskon")
    private Integer diskon;
    @Column(name = "ppn")
    private Integer ppn;
    @Column(name = "total_bayar")
    private Integer totalBayar;
    @Column(name = "bayar")
    private Integer bayar;
    @Column(name = "kembali")
    private Integer kembali;
    @Size(max = 3)
    @Column(name = "kode_pelanggan")
    private String kodePelanggan;
    @Size(max = 30)
    @Column(name = "nama_pengguna")
    private String namaPengguna;

    public Tpenjualan() {
    }

    public Tpenjualan(String noNota) {
        this.noNota = noNota;
    }

    public String getNoNota() {
        return noNota;
    }

    public void setNoNota(String noNota) {
        this.noNota = noNota;
    }

    public Date getTglNota() {
        return tglNota;
    }

    public void setTglNota(Date tglNota) {
        this.tglNota = tglNota;
    }

    public Integer getSubBayar() {
        return subBayar;
    }

    public void setSubBayar(Integer subBayar) {
        this.subBayar = subBayar;
    }

    public Integer getDiskon() {
        return diskon;
    }

    public void setDiskon(Integer diskon) {
        this.diskon = diskon;
    }

    public Integer getPpn() {
        return ppn;
    }

    public void setPpn(Integer ppn) {
        this.ppn = ppn;
    }

    public Integer getTotalBayar() {
        return totalBayar;
    }

    public void setTotalBayar(Integer totalBayar) {
        this.totalBayar = totalBayar;
    }

    public Integer getBayar() {
        return bayar;
    }

    public void setBayar(Integer bayar) {
        this.bayar = bayar;
    }

    public Integer getKembali() {
        return kembali;
    }

    public void setKembali(Integer kembali) {
        this.kembali = kembali;
    }

    public String getKodePelanggan() {
        return kodePelanggan;
    }

    public void setKodePelanggan(String kodePelanggan) {
        this.kodePelanggan = kodePelanggan;
    }

    public String getNamaPengguna() {
        return namaPengguna;
    }

    public void setNamaPengguna(String namaPengguna) {
        this.namaPengguna = namaPengguna;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noNota != null ? noNota.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tpenjualan)) {
            return false;
        }
        Tpenjualan other = (Tpenjualan) object;
        if ((this.noNota == null && other.noNota != null) || (this.noNota != null && !this.noNota.equals(other.noNota))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.Tpenjualan[ noNota=" + noNota + " ]";
    }
    
}
