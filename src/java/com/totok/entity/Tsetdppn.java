/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "tsetdppn")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsetdppn.findAll", query = "SELECT t FROM Tsetdppn t"),
    @NamedQuery(name = "Tsetdppn.findByNo", query = "SELECT t FROM Tsetdppn t WHERE t.no = :no"),
    @NamedQuery(name = "Tsetdppn.findByDisc", query = "SELECT t FROM Tsetdppn t WHERE t.disc = :disc"),
    @NamedQuery(name = "Tsetdppn.findByPpn", query = "SELECT t FROM Tsetdppn t WHERE t.ppn = :ppn")})
public class Tsetdppn implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "no")
    private Integer no;
    @Column(name = "disc")
    private Integer disc;
    @Column(name = "ppn")
    private Integer ppn;

    public Tsetdppn() {
    }

    public Tsetdppn(Integer no) {
        this.no = no;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public Integer getDisc() {
        return disc;
    }

    public void setDisc(Integer disc) {
        this.disc = disc;
    }

    public Integer getPpn() {
        return ppn;
    }

    public void setPpn(Integer ppn) {
        this.ppn = ppn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (no != null ? no.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsetdppn)) {
            return false;
        }
        Tsetdppn other = (Tsetdppn) object;
        if ((this.no == null && other.no != null) || (this.no != null && !this.no.equals(other.no))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.Tsetdppn[ no=" + no + " ]";
    }
    
}
