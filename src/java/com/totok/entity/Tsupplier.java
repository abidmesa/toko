/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "tsupplier")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsupplier.findAll", query = "SELECT t FROM Tsupplier t"),
    @NamedQuery(name = "Tsupplier.findByKodeSupplier", query = "SELECT t FROM Tsupplier t WHERE t.kodeSupplier = :kodeSupplier"),
    @NamedQuery(name = "Tsupplier.findByNamaSupplier", query = "SELECT t FROM Tsupplier t WHERE t.namaSupplier = :namaSupplier"),
    @NamedQuery(name = "Tsupplier.findByAlamat", query = "SELECT t FROM Tsupplier t WHERE t.alamat = :alamat"),
    @NamedQuery(name = "Tsupplier.findByKota", query = "SELECT t FROM Tsupplier t WHERE t.kota = :kota"),
    @NamedQuery(name = "Tsupplier.findByTelepon", query = "SELECT t FROM Tsupplier t WHERE t.telepon = :telepon"),
    @NamedQuery(name = "Tsupplier.findByFax", query = "SELECT t FROM Tsupplier t WHERE t.fax = :fax"),
    @NamedQuery(name = "Tsupplier.findByKontak", query = "SELECT t FROM Tsupplier t WHERE t.kontak = :kontak")})
public class Tsupplier implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "kode_supplier")
    private String kodeSupplier;
    @Size(max = 35)
    @Column(name = "nama_supplier")
    private String namaSupplier;
    @Size(max = 50)
    @Column(name = "alamat")
    private String alamat;
    @Size(max = 30)
    @Column(name = "kota")
    private String kota;
    @Size(max = 15)
    @Column(name = "telepon")
    private String telepon;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 25)
    @Column(name = "fax")
    private String fax;
    @Size(max = 35)
    @Column(name = "kontak")
    private String kontak;

    public Tsupplier() {
    }

    public Tsupplier(String kodeSupplier) {
        this.kodeSupplier = kodeSupplier;
    }

    public String getKodeSupplier() {
        return kodeSupplier;
    }

    public void setKodeSupplier(String kodeSupplier) {
        this.kodeSupplier = kodeSupplier;
    }

    public String getNamaSupplier() {
        return namaSupplier;
    }

    public void setNamaSupplier(String namaSupplier) {
        this.namaSupplier = namaSupplier;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getKontak() {
        return kontak;
    }

    public void setKontak(String kontak) {
        this.kontak = kontak;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kodeSupplier != null ? kodeSupplier.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsupplier)) {
            return false;
        }
        Tsupplier other = (Tsupplier) object;
        if ((this.kodeSupplier == null && other.kodeSupplier != null) || (this.kodeSupplier != null && !this.kodeSupplier.equals(other.kodeSupplier))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.totok.entity.Tsupplier[ kodeSupplier=" + kodeSupplier + " ]";
    }
    
}
