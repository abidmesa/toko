/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "v_labaperitem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VLabaperitem.findAll", query = "SELECT v FROM VLabaperitem v"),
    @NamedQuery(name = "VLabaperitem.findByTanggal", query = "SELECT v FROM VLabaperitem v WHERE v.tanggal = :tanggal"),
    @NamedQuery(name = "VLabaperitem.findByKodeBarang", query = "SELECT v FROM VLabaperitem v WHERE v.kodeBarang = :kodeBarang"),
    @NamedQuery(name = "VLabaperitem.findByNamaBarang", query = "SELECT v FROM VLabaperitem v WHERE v.namaBarang = :namaBarang"),
    @NamedQuery(name = "VLabaperitem.findByHpokokBeli", query = "SELECT v FROM VLabaperitem v WHERE v.hpokokBeli = :hpokokBeli"),
    @NamedQuery(name = "VLabaperitem.findByHargaJual", query = "SELECT v FROM VLabaperitem v WHERE v.hargaJual = :hargaJual"),
    @NamedQuery(name = "VLabaperitem.findByLaba", query = "SELECT v FROM VLabaperitem v WHERE v.laba = :laba"),
    @NamedQuery(name = "VLabaperitem.findByItem", query = "SELECT v FROM VLabaperitem v WHERE v.item = :item"),
    @NamedQuery(name = "VLabaperitem.findByJmlLaba", query = "SELECT v FROM VLabaperitem v WHERE v.jmlLaba = :jmlLaba"),
    @NamedQuery(name = "VLabaperitem.findByPotongan", query = "SELECT v FROM VLabaperitem v WHERE v.potongan = :potongan"),
    @NamedQuery(name = "VLabaperitem.findByTotalLaba", query = "SELECT v FROM VLabaperitem v WHERE v.totalLaba = :totalLaba")})
public class VLabaperitem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "tanggal")
    @Temporal(TemporalType.DATE)
    private Date tanggal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "kode_barang")
    private String kodeBarang;
    @Size(max = 50)
    @Column(name = "nama_barang")
    private String namaBarang;
    @Column(name = "hpokok_beli")
    private Integer hpokokBeli;
    @Basic(optional = false)
    @NotNull
    @Column(name = "harga_jual")
    private int hargaJual;
    @Column(name = "laba")
    private BigInteger laba;
    @Column(name = "item")
    private BigInteger item;
    @Column(name = "jml_laba")
    private BigInteger jmlLaba;
    @Column(name = "potongan")
    private BigInteger potongan;
    @Column(name = "total_laba")
    private BigInteger totalLaba;

    public VLabaperitem() {
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public Integer getHpokokBeli() {
        return hpokokBeli;
    }

    public void setHpokokBeli(Integer hpokokBeli) {
        this.hpokokBeli = hpokokBeli;
    }

    public int getHargaJual() {
        return hargaJual;
    }

    public void setHargaJual(int hargaJual) {
        this.hargaJual = hargaJual;
    }

    public BigInteger getLaba() {
        return laba;
    }

    public void setLaba(BigInteger laba) {
        this.laba = laba;
    }

    public BigInteger getItem() {
        return item;
    }

    public void setItem(BigInteger item) {
        this.item = item;
    }

    public BigInteger getJmlLaba() {
        return jmlLaba;
    }

    public void setJmlLaba(BigInteger jmlLaba) {
        this.jmlLaba = jmlLaba;
    }

    public BigInteger getPotongan() {
        return potongan;
    }

    public void setPotongan(BigInteger potongan) {
        this.potongan = potongan;
    }

    public BigInteger getTotalLaba() {
        return totalLaba;
    }

    public void setTotalLaba(BigInteger totalLaba) {
        this.totalLaba = totalLaba;
    }
    
}
