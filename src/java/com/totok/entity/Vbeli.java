/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "vbeli")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vbeli.findAll", query = "SELECT v FROM Vbeli v"),
    @NamedQuery(name = "Vbeli.findByNoPembelian", query = "SELECT v FROM Vbeli v WHERE v.noPembelian = :noPembelian"),
    @NamedQuery(name = "Vbeli.findByNoFaktur", query = "SELECT v FROM Vbeli v WHERE v.noFaktur = :noFaktur"),
    @NamedQuery(name = "Vbeli.findByTglPembelian", query = "SELECT v FROM Vbeli v WHERE v.tglPembelian = :tglPembelian"),
    @NamedQuery(name = "Vbeli.findByKodeSupplier", query = "SELECT v FROM Vbeli v WHERE v.kodeSupplier = :kodeSupplier"),
    @NamedQuery(name = "Vbeli.findByType", query = "SELECT v FROM Vbeli v WHERE v.type = :type"),
    @NamedQuery(name = "Vbeli.findByTotal", query = "SELECT v FROM Vbeli v WHERE v.total = :total"),
    @NamedQuery(name = "Vbeli.findByDiskon", query = "SELECT v FROM Vbeli v WHERE v.diskon = :diskon"),
    @NamedQuery(name = "Vbeli.findByTermin", query = "SELECT v FROM Vbeli v WHERE v.termin = :termin"),
    @NamedQuery(name = "Vbeli.findByBatasTglDiskon", query = "SELECT v FROM Vbeli v WHERE v.batasTglDiskon = :batasTglDiskon"),
    @NamedQuery(name = "Vbeli.findByTglJtmp", query = "SELECT v FROM Vbeli v WHERE v.tglJtmp = :tglJtmp"),
    @NamedQuery(name = "Vbeli.findByLunas", query = "SELECT v FROM Vbeli v WHERE v.lunas = :lunas"),
    @NamedQuery(name = "Vbeli.findByNamaPengguna", query = "SELECT v FROM Vbeli v WHERE v.namaPengguna = :namaPengguna"),
    @NamedQuery(name = "Vbeli.findByNamaSupplier", query = "SELECT v FROM Vbeli v WHERE v.namaSupplier = :namaSupplier")})
public class Vbeli implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "no_pembelian")
    private String noPembelian;
    @Size(max = 20)
    @Column(name = "no_faktur")
    private String noFaktur;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tgl_pembelian")
    @Temporal(TemporalType.DATE)
    private Date tglPembelian;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "kode_supplier")
    private String kodeSupplier;
    @Size(max = 10)
    @Column(name = "type")
    private String type;
    @Column(name = "total")
    private Integer total;
    @Column(name = "diskon")
    private Integer diskon;
    @Size(max = 20)
    @Column(name = "termin")
    private String termin;
    @Column(name = "batas_tgl_diskon")
    @Temporal(TemporalType.DATE)
    private Date batasTglDiskon;
    @Column(name = "tgl_jtmp")
    @Temporal(TemporalType.DATE)
    private Date tglJtmp;
    @Column(name = "lunas")
    private Character lunas;
    @Size(max = 35)
    @Column(name = "nama_pengguna")
    private String namaPengguna;
    @Size(max = 35)
    @Column(name = "nama_supplier")
    private String namaSupplier;

    public Vbeli() {
    }

    public String getNoPembelian() {
        return noPembelian;
    }

    public void setNoPembelian(String noPembelian) {
        this.noPembelian = noPembelian;
    }

    public String getNoFaktur() {
        return noFaktur;
    }

    public void setNoFaktur(String noFaktur) {
        this.noFaktur = noFaktur;
    }

    public Date getTglPembelian() {
        return tglPembelian;
    }

    public void setTglPembelian(Date tglPembelian) {
        this.tglPembelian = tglPembelian;
    }

    public String getKodeSupplier() {
        return kodeSupplier;
    }

    public void setKodeSupplier(String kodeSupplier) {
        this.kodeSupplier = kodeSupplier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getDiskon() {
        return diskon;
    }

    public void setDiskon(Integer diskon) {
        this.diskon = diskon;
    }

    public String getTermin() {
        return termin;
    }

    public void setTermin(String termin) {
        this.termin = termin;
    }

    public Date getBatasTglDiskon() {
        return batasTglDiskon;
    }

    public void setBatasTglDiskon(Date batasTglDiskon) {
        this.batasTglDiskon = batasTglDiskon;
    }

    public Date getTglJtmp() {
        return tglJtmp;
    }

    public void setTglJtmp(Date tglJtmp) {
        this.tglJtmp = tglJtmp;
    }

    public Character getLunas() {
        return lunas;
    }

    public void setLunas(Character lunas) {
        this.lunas = lunas;
    }

    public String getNamaPengguna() {
        return namaPengguna;
    }

    public void setNamaPengguna(String namaPengguna) {
        this.namaPengguna = namaPengguna;
    }

    public String getNamaSupplier() {
        return namaSupplier;
    }

    public void setNamaSupplier(String namaSupplier) {
        this.namaSupplier = namaSupplier;
    }
    
}
