/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bowo.Susanto
 */
@Entity
@Table(name = "vdata_barang")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VdataBarang.findAll", query = "SELECT v FROM VdataBarang v"),
    @NamedQuery(name = "VdataBarang.findByKodeBarang", query = "SELECT v FROM VdataBarang v WHERE v.kodeBarang = :kodeBarang"),
    @NamedQuery(name = "VdataBarang.findByNoJenis", query = "SELECT v FROM VdataBarang v WHERE v.noJenis = :noJenis"),
    @NamedQuery(name = "VdataBarang.findByJenisBarang", query = "SELECT v FROM VdataBarang v WHERE v.jenisBarang = :jenisBarang"),
    @NamedQuery(name = "VdataBarang.findByNamaBarang", query = "SELECT v FROM VdataBarang v WHERE v.namaBarang = :namaBarang"),
    @NamedQuery(name = "VdataBarang.findByHargaBeli", query = "SELECT v FROM VdataBarang v WHERE v.hargaBeli = :hargaBeli"),
    @NamedQuery(name = "VdataBarang.findByHargaJual", query = "SELECT v FROM VdataBarang v WHERE v.hargaJual = :hargaJual"),
    @NamedQuery(name = "VdataBarang.findByStok", query = "SELECT v FROM VdataBarang v WHERE v.stok = :stok"),
    @NamedQuery(name = "VdataBarang.findByMerk", query = "SELECT v FROM VdataBarang v WHERE v.merk = :merk"),
    @NamedQuery(name = "VdataBarang.findBySatuan", query = "SELECT v FROM VdataBarang v WHERE v.satuan = :satuan"),
    @NamedQuery(name = "VdataBarang.findByHargaJual1", query = "SELECT v FROM VdataBarang v WHERE v.hargaJual1 = :hargaJual1"),
    @NamedQuery(name = "VdataBarang.findByIsi", query = "SELECT v FROM VdataBarang v WHERE v.isi = :isi")})
public class VdataBarang implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "kode_barang")
    private String kodeBarang;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "no_jenis")
    private String noJenis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 35)
    @Column(name = "jenis_barang")
    private String jenisBarang;
    @Size(max = 50)
    @Column(name = "nama_barang")
    private String namaBarang;
    @Column(name = "harga_beli")
    private Integer hargaBeli;
    @Column(name = "harga_jual")
    private Integer hargaJual;
    @Basic(optional = false)
    @NotNull
    @Column(name = "stok")
    private short stok;
    @Size(max = 50)
    @Column(name = "merk")
    private String merk;
    @Size(max = 20)
    @Column(name = "satuan")
    private String satuan;
    @Lob
    @Column(name = "satuan1")
    private byte[] satuan1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "harga_jual1")
    private int hargaJual1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isi")
    private short isi;

    public VdataBarang() {
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getNoJenis() {
        return noJenis;
    }

    public void setNoJenis(String noJenis) {
        this.noJenis = noJenis;
    }

    public String getJenisBarang() {
        return jenisBarang;
    }

    public void setJenisBarang(String jenisBarang) {
        this.jenisBarang = jenisBarang;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public Integer getHargaBeli() {
        return hargaBeli;
    }

    public void setHargaBeli(Integer hargaBeli) {
        this.hargaBeli = hargaBeli;
    }

    public Integer getHargaJual() {
        return hargaJual;
    }

    public void setHargaJual(Integer hargaJual) {
        this.hargaJual = hargaJual;
    }

    public short getStok() {
        return stok;
    }

    public void setStok(short stok) {
        this.stok = stok;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public byte[] getSatuan1() {
        return satuan1;
    }

    public void setSatuan1(byte[] satuan1) {
        this.satuan1 = satuan1;
    }

    public int getHargaJual1() {
        return hargaJual1;
    }

    public void setHargaJual1(int hargaJual1) {
        this.hargaJual1 = hargaJual1;
    }

    public short getIsi() {
        return isi;
    }

    public void setIsi(short isi) {
        this.isi = isi;
    }
    
}
