/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.service;

import com.totok.entity.Karyawan;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Bowo.Susanto
 */
public interface KaryawanDAO {
    Boolean insert(Karyawan karyawan)throws SQLException;
    List<Karyawan> getAllKaryawan()throws SQLException;
    Boolean update(Karyawan karyawan)throws SQLException;
    Boolean delete(Karyawan karyawan)throws SQLException;
}
