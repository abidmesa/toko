/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.service;

import com.totok.entity.Karyawan;
import com.totok.entity.Tpengguna;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Bowo.Susanto
 */
public interface PenggunaDAO {
    Boolean insert(Tpengguna tpengguna)throws SQLException;
    List<Tpengguna> getAllPengguna()throws SQLException;
    Boolean update(Tpengguna tpengguna)throws SQLException;
    Boolean delete(Tpengguna tpengguna)throws SQLException;
    Boolean getPengguna(Tpengguna tpengguna)throws SQLException;
}
