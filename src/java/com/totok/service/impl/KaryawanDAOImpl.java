/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.service.impl;

import com.totok.entity.Karyawan;
import com.totok.service.KaryawanDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bowo.Susanto
 */
public class KaryawanDAOImpl implements KaryawanDAO{
    private Connection connection;
    public KaryawanDAOImpl(Connection connection) {
        this.connection = connection;
    }
    
    @Override
    public Boolean insert(Karyawan karyawan) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Karyawan> getAllKaryawan() throws SQLException {
        
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection.setAutoCommit(false);
 
            statement = connection.prepareStatement("SELECT * FROM karyawan");
 
            rs = statement.executeQuery();
            List<Karyawan> list = new ArrayList<Karyawan>();
            while (rs.next()) {
                Karyawan karyawan = new Karyawan();
                karyawan.setIdKaryawan(rs.getInt("id"));
                karyawan.setNip(rs.getString("nip"));
                karyawan.setNama(rs.getString("nama"));
                karyawan.setAlamat(rs.getString("alamat"));
                karyawan.setStatus(rs.getString("status"));
                karyawan.setJenisKelamin(rs.getString("jenis_kelamin"));
                karyawan.setTanggalLahir(rs.getDate("tanggal_lahir"));
                karyawan.setTempatLahir(rs.getString("tempat_lahir"));
                list.add(karyawan);
            }
 
            connection.commit();
            return list;
        } catch (SQLException exception) {
            throw exception;
        } finally {
            try {
                connection.setAutoCommit(true);
                if (rs != null) {
                    rs.close();
                }
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException exception) {
                throw exception;
            }
        }
        
    }

    @Override
    public Boolean update(Karyawan karyawan) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean delete(Karyawan karyawan) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
