/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.service.impl;

import com.totok.entity.Karyawan;
import com.totok.entity.Tpengguna;
import com.totok.service.PenggunaDAO;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bowo.Susanto
 */
public class PenggunaDAOImpl implements PenggunaDAO{
    private Connection connection;
    public PenggunaDAOImpl(Connection connection) {
        this.connection = connection;
    }
    
    @Override
    public Boolean insert(Tpengguna tpengguna) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean update(Tpengguna tpengguna) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean delete(Tpengguna tpengguna) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean getPengguna(Tpengguna tpengguna) throws SQLException {        
        Integer totalRow;

        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM tpengguna WHERE nama_pengguna=? AND password=?")) {
            statement.setString(1, tpengguna.getNamaPengguna());
            statement.setString(2, tpengguna.getPassword());
            try (ResultSet rs = statement.executeQuery()) {
                rs.next();
                totalRow = rs.getInt(1);
            }
        }
        return totalRow > 0;
    }

    @Override
    public List<Tpengguna> getAllPengguna() throws SQLException {
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection.setAutoCommit(false);
 
            statement = connection.prepareStatement("SELECT * FROM tpengguna");
 
            rs = statement.executeQuery();
            List<Tpengguna> list = new ArrayList<Tpengguna>();
            while (rs.next()) {
                Tpengguna tpengguna = new Tpengguna();
                tpengguna.setAlamat(rs.getString("alamat"));
                tpengguna.setIdPengguna(rs.getString("id_pengguna"));
                tpengguna.setLevel(rs.getString("level"));
                tpengguna.setNamaPengguna(rs.getString("nama_pengguna"));
                tpengguna.setPassword(rs.getString("password"));
                tpengguna.setTelepon(rs.getString("telepon"));
                list.add(tpengguna);
            }
 
            connection.commit();
            return list;
        } catch (SQLException exception) {
            throw exception;
        } finally {
            try {
                connection.setAutoCommit(true);
                if (rs != null) {
                    rs.close();
                }
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException exception) {
                throw exception;
            }
        }
    }
    
}
