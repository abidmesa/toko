/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.totok.util;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import com.totok.entity.Tpengguna;
import com.totok.service.KaryawanDAO;
import com.totok.service.PenggunaDAO;
import com.totok.service.impl.KaryawanDAOImpl;
import com.totok.service.impl.PenggunaDAOImpl;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Bowo.Susanto
 */
public class DaoFactory {
    private static KaryawanDAO karyawanService;
    private static PenggunaDAO penggunaService;
    public DaoFactory(){
    }
    private static  Connection connection;
    public static Connection getConnection() throws SQLException {
        if (connection == null) {
            MysqlDataSource dataSource = new MysqlDataSource();
            dataSource.setUrl("jdbc:mysql://localhost:3306/tokopakaian");
            dataSource.setUser("root");
            dataSource.setPassword("root");

            connection = dataSource.getConnection();
        }
        return connection;
    }

    public static PenggunaDAO getPenggunaService() throws SQLException {
        if(penggunaService==null){
            penggunaService = new PenggunaDAOImpl(getConnection());
        }
        return penggunaService;
    }

    public static KaryawanDAO getKaryawanService() throws SQLException {
        if(karyawanService==null){
            karyawanService = new KaryawanDAOImpl(getConnection());
        }
        return karyawanService;
    }
    
}
